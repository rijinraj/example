<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\examplecontroller;
use App\Http\Controllers\userlogincontroller;
use App\Http\Controllers\usercontroller;
use App\Http\Controllers\sessioncontroller;
use App\Http\Controllers\adduserscontroller;
use App\Http\Controllers\fileuploadcontroller;
use App\Http\Controllers\viewuserscontroller;
use App\Http\Controllers\Customauthcontroller;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});
Route::get("controllerexample",[examplecontroller::class,'example']);

Route::get('/about', function () {
    return view('about');
});
Route::post("user",[userlogincontroller::class,'login']);
Route::view('login',"user");
Route::get('/contact', function () {
    return view('contact');
});
Route::post("sample",[usercontroller::class,'getdata']);


Route::post("userl",[sessioncontroller::class,'userlogin']);
//Route::view('userlogin',"userlogin");
Route::get('/userlogin', function () {
    return view('userlogin');
});
Route::view('session',"session");

Route::get('/userlogin', function () {
    if(session()->has('username'))
    {
        return redirect('session');
    }
    return view('userlogin');
   
    }); 

Route::get('/logout', function () {
    if(session()->has('username'))
    {
        session()->pull('username');
    }
    return redirect('userlogin');
   
    }); 


    
       
    Route::post("addmem",[adduserscontroller::class,'addmembers']);
    Route::get('/useradd', function(){
        return view('useradd');
    });

    route::get('/fileupload', function(){
        return view('fileupload');
    });
    route::post("fileup",[fileuploadcontroller::class,'fileupload']);

    Route::get("viewuser",[viewuserscontroller::class,'show']);
    Route::get("delete/{id}",[viewuserscontroller::class,'delete']);
    Route::get("edit/{id}",[viewuserscontroller::class,'viewData']);
    Route::post("/edit",[viewuserscontroller::class,'update']);
    Route::get("view/{id}",[viewuserscontroller::class,'viewUser']);
    Route::get("/authlogin",[Customauthcontroller::class,'userLogin'])->middleware('alreadyloggedin');
    Route::get("/register",[Customauthcontroller::class,'userRegister'])->middleware('alreadyloggedin');
    Route::post("/register-user",[Customauthcontroller::class,'registeruser'])->name('register-user');
    Route::post("login-user",[Customauthcontroller::class,'loginuser'])->name('login-user');
    Route::get("/dashboard",[Customauthcontroller::class,'dashboard'])->middleware('isloggedin');
    Route::get("/signout",[Customauthcontroller::class,'signout']);






