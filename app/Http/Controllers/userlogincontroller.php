<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class userlogincontroller extends Controller
{
    //
    function login(Request $req)
    {
        $req->validate(['username'=>'required | max:10',
        'password'=>'required | min:6']);
        return $req->input();
    }
}
