<?php

namespace App\Http\Controllers;
use Session;


use App\Models\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Customauthcontroller extends Controller
{
    //
    public function userLogin()
    {
        return view("userauthlogin");
    }
    public function userRegister()
    {
        return view("userauthregister");
    }
    public function registeruser(Request $req)
    {
        $req->validate(
            ['username'=>'required',
            'email'=>'required|email|unique:members',
        'password'=>'required | min:6'
        ]);
        $member=new Member();
        $member->name=$req->username;
        $member->email=$req->email;
        $member->password=Hash::make($req->password);
        $res=$member->save();
        if($res)
        {
            return back()->with('success','you have successfully registered');
        }
        else{
            return back()->with('fail','something wrong');
        }
        
    }
    public function loginuser(Request $req)
    {
        $req->validate(
            [
            'email'=>'required|email',
        'password'=>'required | min:6'
        ]);
        $member=Member::where('email','=',$req->email)->first();
        if($member)
        {
            if(Hash::check($req->password,$member->password)){
                $req->session()->put('loginId',$member->id);
                return redirect('dashboard');
            }
            else {
                return back()->with('fail','password not matches');
            }
        }
            else {
                return back()->with('fail','email not registered');
            }
        
    }
    public function dashboard()
    {
        $data=array();
         if(Session::has('loginId'))
        {
          $data=Member::where('id','=',Session::get('loginId'))->first();

        }
        return view('dashboard',compact('data'));
    }
    public function signout()
    {
        if(Session::has('loginId')){
           Session::pull('loginId');
           return redirect('authlogin');
        }
        
    }
}
