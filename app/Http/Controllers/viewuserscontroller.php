<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\user;
use Illuminate\Http\Request;

class viewuserscontroller extends Controller
{
    //
    function show()
    {
       $data=user::all();
       
        return view('viewuser',['users'=>$data]);
    }
    function delete($id)
    {
       /*try {
       $data=user::where('id',$id)->first();
       $data->delete();
       }
       catch (Exception $e)
       {
          logger($e);
          throw ($e);
       }

       //dd($data);
       return redirect()->route('viewuser')->with('success','deleted successfully!');
    }*/
    $data=user::find($id);
    $data->delete();
     return redirect('viewuser');
     }
 

   function viewData($id)
      {
         $data=user::find($id);
         return view('edit',['data'=>$data]);

      }
   function update(Request $req)
      {
         $data=user::find($req->id);
         $data->name=$req->username;
         $data->place=$req->place;
         $data->save();
         return redirect('viewuser');
   }

function viewUser($id)
      {
         $data=user::find($id);
         return view('viewuserprofile',['data'=>$data]);

      }
   }
