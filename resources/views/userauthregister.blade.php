<html>
    <head>
        <title>user registration</title>
    </head>
    <body>
        <form action="{{route('register-user')}}" method="POST">
            @csrf
            <h1>REGISTRATION</h1>
            <table height="300px">
            @if(Session::has('success'))
                <tr><td style="color:green">{{Session::get('success')}}</tr>
                @endif
                @if(Session::has('fail'))
                <tr><td style="color:red">{{Session::get('fail')}}</tr>
                @endif

            <tr><td>
            <label>Username</label></td>
            <td><input type="text" id="username" name="username" value="{{old('username')}}">
            <span style="color:red"> @error('username'){{$message}} @enderror</span></td>
</tr>
        <tr>
            <td><label>password</label></td>
            <td><input type="password" id="password" name="password" value="">
            <span style="color:red">@error('password'){{$message}} @enderror</span></td>
</tr>    
           <tr><td> <label>email</label></td>
            <td><input type="text" id="email" name="email" value="{{old('email')}}">
            <span style="color:red">@error('email'){{$message}} @enderror</span></td></tr>
            <tr><td><button type="submit">Register</button></td>
            <td><a href="authlogin">already registered!! login here</a></td></tr>

            </table>
        </form>
    </body>
    </html>